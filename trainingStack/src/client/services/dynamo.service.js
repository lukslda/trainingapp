const dynamo = require('ebased/service/storage/dynamo');

async function createClientService(commandPayload) {
  await dynamo.putItem({
    TableName: process.env.CLIENTS_TABLE,
    Item: commandPayload
  });
}

async function updateClientService(payload) {
    await dynamo.updateItem(payload);
}

module.exports = { createClientService, updateClientService };
const { calculateAge } = require("../helper/calculateAge.helper");
const { generateCreditCard } = require("../helper/generateCreditCard.helper");
const { updateClientService } = require('../services/dynamo.service');

async function createCardDomain (eventPayLoad, commandMeta) {
  const data = JSON.parse(eventPayLoad.Message);
  const birth = data.birth;
  const age = calculateAge(birth);
  const creditCard = generateCreditCard(age, data);
  await updateClientService(creditCard);
  return {
    statusCode: 200,
    body: "credit card created",
  };
}

module.exports = { createCardDomain };

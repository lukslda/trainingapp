const {
  CreateClientValidation,
} = require("../schema/input/CreateClientValidation.input");
const { createClientService } = require("../services/createClient.service");
const {
  publishClientCreated,
} = require("../services/publishClientCreated.service");
const { ClientCreated } = require("../schema/event/clientCreated.event");
const { calculateAge } = require("../helper/calculateAge.helper");

async function createClientDomain(commandPayLoad, commandMeta) {
  new CreateClientValidation(commandPayLoad, commandMeta);
  const age = calculateAge(commandPayLoad.birth);
  if (age < 18 || age > 65 ) {
    return {
      statusCode: 400,
      body: JSON.stringify({msg: "Client must be between 18 and 65 years old"}),
    };
  }
  await createClientService(commandPayLoad);
  await publishClientCreated(new ClientCreated(commandPayLoad, commandMeta));
  return {
    statusCode: 200,
    body: "Success"
  };
};

module.exports = { createClientDomain };

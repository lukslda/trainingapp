const { generateGift } = require("../helper/generateGift.helper");
const { updateClientService } = require('../services/dynamo.service');

async function createGiftDomain (eventPayLoad, commandMeta) {
  const data = JSON.parse(eventPayLoad.Message);
  const birth = data.birth;
  const gift = generateGift(birth, data)
  await updateClientService(gift);
  return {
    statusCode: 200,
    body: "Gift created succesfully",
  };
}

module.exports = { createGiftDomain };

const { InputValidation } = require("ebased/schema/inputValidation");

class CreateClientValidation extends InputValidation {
  constructor(payload, meta) {
    super({
      source: meta.source,
      payload: payload,
      type: "CLIENT.LAMBDA",
      specversion: "1.0.0",
      schema: {
        strict : false,
        dni: { type: String, required: true },
        name: { type: String , required: true},
        lastName: { type: String , required: true},
        birth: { type: String , required: true}
      }
    });
  }
}

module.exports = {
  CreateClientValidation,
};

const { DownstreamEvent } = require('ebased/schema/downstreamEvent');

class ClientCreated extends DownstreamEvent {
  constructor(payload, meta) {
    super({
      payload: payload,
      meta: meta,
      type: 'CLIENT.LAMBDA',
      specversion: 'v1.0.0',
      schema: {
        name: { type: String, required: true },
        lastName: { type: String, required: true },
        dni: { type: String, required: true },
        birth: { type: String, required: true }
      }
    })
  }
}

module.exports = { ClientCreated };

const calculateAge = birthday => {
  const birthDate = new Date(birthday);
  const ageDifMs = Date.now() - birthDate.getTime();
  const ageDate = new Date(ageDifMs);
  const age = Math.abs(ageDate.getFullYear() - 1970);
  return age;
}

module.exports = { calculateAge };
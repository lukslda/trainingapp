const TABLE_NAME = process.env.CLIENTS_TABLE;

function randomNumber(minimum, maximum) {
  return Math.round(Math.random() * (maximum - minimum) + minimum);
}

const generateCreditCard = (age, dataClient) => {
  const creditCardNumber = `${randomNumber(0000, 9999)}-${randomNumber(
    0000,
    9999
  )}-${randomNumber(0000, 9999)}-${randomNumber(0000, 9999)}`;
  const expirationDate = `${randomNumber(01, 12)}/${randomNumber(21, 35)}`;
  const securityCode = `${randomNumber(000, 999)}`;
  let type = age > 45 ? "Gold" : "Classic";
  const cardData = {
    creditCardNumber,
    expirationDate,
    securityCode,
    type
  }
  const params = {
    TableName: TABLE_NAME,
    Key: { dni: dataClient.dni },
    UpdateExpression: 'SET#C=:c',
    ExpressionAttributeNames: { '#C': 'creditCard' },
    ExpressionAttributeValues: { ':c': {} },
    ReturnValues: 'ALL_NEW',
  };
  Object.keys(cardData).forEach((key) => {
    params.ExpressionAttributeValues[':c'][key] = cardData[key];
  });

  if (!creditCardNumber || !expirationDate || !securityCode || !type ) {
    return {
      statusCode: 400,
      body: JSON.stringify({
        msg:"ERROR: failed to create credit card", 
      })
    };
  }
  return params;
};

module.exports = { generateCreditCard };
